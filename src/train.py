import os

import click
import torch
from dotenv import load_dotenv
from lightning import Trainer
from lightning.pytorch.callbacks import ModelCheckpoint
from lightning.pytorch.loggers import MLFlowLogger
from mlflow.pytorch import log_model

import src.config as config
from src.config import (
    CHECKPOINT_EVERY,
    CHECKPOINTS_PATH,
    DEMOS_PATH,
    OUTPUTS_PATH,
    CKPT_PATH,
    RAW_DATA_DIR,
    MODELS_DIR,
)
from src.data.datamodule import AudioGenerationDatamodule
from src.model.model import AudioGenerationModel, DemoCallback


@click.command()
@click.option("--epochs", default=1, help="Number of training epochs")
def train(epochs):
    load_dotenv()
    torch.set_float32_matmul_precision("high")

    CHECKPOINTS_PATH.mkdir(parents=True, exist_ok=True)
    DEMOS_PATH.mkdir(parents=True, exist_ok=True)

    datamodule = AudioGenerationDatamodule(RAW_DATA_DIR)
    model = AudioGenerationModel()

    logger = MLFlowLogger(
        experiment_name="train_audio_diffusion",
        tracking_uri=os.getenv("MLFLOW_TRACKING_URI"),
        log_model=True,
    )

    logger.log_hyperparams(
        {k: v for k, v in vars(config).items() if not k.startswith("__")}
    )

    trainer = Trainer(
        accelerator="gpu",
        default_root_dir=OUTPUTS_PATH,
        log_every_n_steps=1,
        max_epochs=epochs,
        logger=logger,
        callbacks=[
            DemoCallback(),
            ModelCheckpoint(CHECKPOINTS_PATH, every_n_train_steps=CHECKPOINT_EVERY),
        ],
    )

    trainer.fit(model, datamodule, ckpt_path=CKPT_PATH)
    trainer.save_checkpoint(MODELS_DIR / "trained_model.ckpt")

    log_model(model, "audio_diffustion", registered_model_name="audio_diffusion")


if __name__ == "__main__":
    train()
