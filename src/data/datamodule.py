from pathlib import Path

from lightning import LightningDataModule
from torch.utils.data import DataLoader

from src.config import BATCH_SIZE, NUM_WORKERS
from src.data.dataset import AudioGenerationDataset


class AudioGenerationDatamodule(LightningDataModule):
    def __init__(self, data_path: Path):
        super().__init__()
        self.data_path = data_path

    def setup(self, stage: str):
        self.dataset = AudioGenerationDataset(self.data_path)

    def train_dataloader(self):
        return DataLoader(
            self.dataset,
            batch_size=BATCH_SIZE,
            shuffle=True,
            num_workers=NUM_WORKERS,
            persistent_workers=True,
        )
