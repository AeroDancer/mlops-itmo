from pathlib import Path

import torch
import torchaudio
from torch.nn.functional import pad
from torch.utils.data import Dataset
from torchaudio.functional import resample

from src.config import SAMPLE_RATE, NUM_SAMPLES


class AudioGenerationDataset(Dataset):
    def __init__(self, data_path: Path):
        super().__init__()

        self.audios = []
        start_times = []
        durations = []
        for audio_path in data_path.iterdir():
            if audio_path.suffix not in (".flac", ".wav"):
                continue
            waveform, sample_rate = torchaudio.load(audio_path)
            if sample_rate != SAMPLE_RATE:
                waveform = resample(waveform, sample_rate, SAMPLE_RATE)
                sample_rate = SAMPLE_RATE
            num_samples = waveform.size(-1)
            pad_size = NUM_SAMPLES - num_samples % NUM_SAMPLES
            waveform = pad(waveform, (0, pad_size), "constant", 0)
            num_samples += pad_size
            duration = num_samples / sample_rate
            offset = 0
            while offset < num_samples:
                audio = waveform[:, offset : offset + NUM_SAMPLES]
                start_time = offset / sample_rate
                self.audios.append([audio, torch.tensor(start_time), torch.tensor(duration)])
                start_times.append(start_time)
                durations.append(duration)
                offset += NUM_SAMPLES

        self.max_start_time = max(start_times)
        self.max_duration = max(durations)
        for audio in self.audios:
            audio[1] /= self.max_start_time
            audio[2] /= self.max_duration

    def __len__(self):
        return len(self.audios)

    def __getitem__(self, index):
        return self.audios[index]
